# IoT Projekt - Sniffer na BLE
Jako IoT projekt jsem realizoval sniffer advertising packetů, který je dekóduje a přeposílá na na server v síti LAN. Primárně je sniffer cílen do mé chytré domácnosti kde sbírá data z čidel LYWSD03MMC a předává je na zpracování do Home Assistant. V praxi ale není problém parsovat data i z dalších senzorů.
## Čidlo LYWSD03MMC
Čidlo LYWSD03MMC je levný senzor teploty a vlhkosti od firmy Xiaomi, který využívá pro přenos dat BLE 4.2 advertising packetů.

![Teplotní čidlo](https://camo.githubusercontent.com/2f8b0990d04d7992cc7792040e00d1850d99e69cc7ac4908419cef37cbca6f62/68747470733a2f2f707676782e6769746875622e696f2f4154435f4d69546865726d6f6d657465722f696d672f4c5957534430334d4d432e706e67)

Při koupi je dodáváno s proprietárním firmware, existuje však také mnohem flexibilnější otevřený [firmware vytvořený komunitou](https://github.com/pvvx/ATC_MiThermometer), který na svých zařízeních používám. Ten umožňuje například úpravu vysílacího výkonu nebo vlastní formát advertising packetů. Ve své domácnosti používám předdefinovaný formát zmíněný v repozitáři. Při něm se posílá advertising packet s UUID 0x181A a velikostí 19 bytů, data jsou posílána jako little-endian. Složení packetu je zapsáno níže (převzato z repozitíře komunitního firmware).

```
uint8_t     size;               // byte 0:                   Velikost packetu, vždy rovna 18
uint8_t     uid;                // byte 1:                   Velikost UID, vždy rovna 0x16 -> 16-bit UUID
uint16_t    UUID;               // byte 2 a 3:               Samotné UUID, vždy rovno 0x181A -> GATT Service 0x181A Environmental Sensing
uint8_t     MAC[6];             // byte 4, 5, 6, 7, 8, 9:    MAC adresa, [0] - lo, .. [6] - hi digits
int16_t     temperature;        // byte 10 a 11:             Teplota s přesností 0.01 stupně
uint16_t    humidity;           // byte 12 a 13:             Vlhkost s přesností 0.01 procenta
uint16_t    battery_mv;         // byte 14 a 15:             Napětí baterie v milivoltech
uint8_t     battery_level;      // byte 16:                  Úroveň nabití baterie v procentech
uint8_t     counter;            // byte 17:                  Počet měření
uint8_t     flags;              // byte 18:                  Stavové bity GPIO_TRG pinu: 
                                                                // bit0: Reed Switch, input
                                                                // bit1: GPIO_TRG pin output value (pull Up/Down)
                                                                // bit2: Output GPIO_TRG pin is controlled according to the set parameters
                                                                // bit3: Temperature trigger event
                                                                // bit4: Humidity trigger event
```

## Přijímací zařízení
Jako přijímač jsem využil zařízení které jsem realizoval v rámci své bakalářské práce. Jedná se o bránu navrženou pro překlad IoT sítí (Zigbee, BLE, Thread) do IP sítě. Zařízení je řízeno dvoujádrovým SoC nRF5340 který se stará o komunikaci s IoT sítí. SoC je doplněn o nRF21540 front-end module, který vylepšuje citlivost přijímače a výkon, v případě vysílání. Připojení do LAN je realizováno přes metalický spoj za pomoci rozhraní WS5100S připojeného k řídícímu mikrokontroléru. Desku je možné napájet z PoE, podporuje standard 802.3 af. Dále je možné desku napájet a komunikovat s ní po USB. Zařízení je vidět na obrázku níže. Schéma zařízení je dostupné [zde](resources/repeat-hw.pdf).

![Obrázek desky zařízení](resources/pcb.png)

Vzhledem k tomu, že nRF5340 dobře podporuje Zephyr RTOS, použil jsem jej pro realizaci programové logiky celého zařízení. Pro zařízení jsem si také vytvořil vlastní definici desky která usnadnila vývoj. Ta je k nalezení ve složce [board_definition](board_definition).

### Driver pro W5100S
Ačkoliv má Zephyr výbornou podporu periferních obvodů, chyběla v něm v mém případě podpora použitého ethernetového rozhraní W5100S. Díky jednoduché rozšiřitelnosti systému jsem však byl schopen vytvořit vlastní driver pro toto rozhraní. Má implementace majoritně využívá existujícího driveru pro velice populární rozhraní W5500. To má ale oproti W5100S více dostupných socketů a paměti. Bylo tedy nutné upravit paměťové
rozsahy. Dále jsem upravil zápis a adresaci některých registrů, které se v rozhraní W5100S nevyskytují a způsob přenosu adresy v SPI rámci. Výsledný softwarový modul driveru je [dostupný v mém repozitáři](https://github.com/WojtaCZ/zephyr-w5100).

### Software
Software, který jsem pro zařízení napsal, kombinuje upravené příklady BLE scanneru a MQTT publisheru. Je k nalezení v tomto adresáři [ve složce ble_to_mqtt](ble_to_mqtt). Software přijímá advertising BLE packety a filtruje je, aby vybral pouze packety z LYWSD03MMC. Dále je packet dekódován a data zapsána do fronty pro zpracování. V druhém vlákně jsou data z fronty odebírána a publishována na MQTT broker ve formátu JSON.

## Home Assistant
MQTT broker běží na mém domácím serveru s Home Assistant. Příklad datového packetu od teplotního čidla přijatého v Home Assistant je vidět níže.

![Packet přijatý na brokeru](resources/broker_packet.png)

### Template senzoru
V Home Assistant jsem následně definoval template senzoru viz YAML snippet níže.
```
mqtt:
  sensor:
    - name: "Teplota"
      state_topic: "repeat/ble/A4:C1:38:98:6C:96"
      suggested_display_precision: 1
      unit_of_measurement: "°C"
      value_template: "{{ value_json.temperature }}"
    - name: "Vlhkost"
      state_topic: "repeat/ble/A4:C1:38:98:6C:96"
      suggested_display_precision: 1
      unit_of_measurement: "%"
      value_template: "{{ value_json.humidity }}"
    - name: "Úroveň baterie"
      state_topic: "repeat/ble/A4:C1:38:98:6C:96"
      suggested_display_precision: 0
      unit_of_measurement: "%"
      value_template: "{{ value_json.batt_percentage }}"
    - name: "Napětí baterie"
      suggested_display_precision: 2
      state_topic: "repeat/ble/A4:C1:38:98:6C:96"
      unit_of_measurement: "V"
      value_template: "{{ value_json.batt_voltage }}"
    - name: "RSSI"
      suggested_display_precision: 0
      state_topic: "repeat/ble/A4:C1:38:98:6C:96"
      unit_of_measurement: "dBm"
      value_template: "{{ value_json.rssi }}"
```

Po načtení konfigurace bylo možné přidat entity na dashboard a vytvořit tak kartu s čidlem.
![Karta čidla](resources/card.png)

# Závěr
Funguje to moc hezky :) v budoucnu bych rád poupravil tak, že bude zařízení přeposílat surová data ze všech dostupných BLE MAC a parsování bude probíhat až na straně Home Assistanta, což zajistí mnohem lepší škálovatelnost a rozšiřitelnost.
